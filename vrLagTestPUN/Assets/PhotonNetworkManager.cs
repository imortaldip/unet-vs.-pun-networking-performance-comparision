﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonNetworkManager : Photon.MonoBehaviour
{
    [SerializeField] private Text connectText;
    [SerializeField] private GameObject player;
    [SerializeField] private Transform spawnPoint;
    

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("v0.1");
    }

    public virtual void OnJoinedLobby()
    {
        Debug.Log("We are in lobby");

        // Join a room if it exists of create one
        PhotonNetwork.JoinOrCreateRoom("New", null, null);
    }

    public virtual void OnJoinedRoom()
    {
        PhotonNetwork.Instantiate(player.name, spawnPoint.position, spawnPoint.rotation, 0);
    }
    // Update is called once per frame
    void Update()
    {
        connectText.text = PhotonNetwork.connectionStateDetailed.ToString();
    }
}
