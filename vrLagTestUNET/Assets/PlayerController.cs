﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour
{
    private NetworkClient nClient;
    private int latency;
    private Text latencyText;

    private void Start()
    {
        nClient = GameObject.Find("Network Manager").GetComponent<NetworkManager>().client;
        latencyText = GameObject.Find("Latency Text").GetComponent<Text>();

        if (isServer && isLocalPlayer)
        {
            Camera camera = Camera.main;
            transform.parent = camera.transform;
            
        }
    }

  
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        ShowLatency();

    }

    void ShowLatency()
    {
        if (isLocalPlayer)
        {
            latency = nClient.GetRTT();
            latencyText.text = latency.ToString();
            latencyText.color = Color.blue;

        }
    }
}


//VR