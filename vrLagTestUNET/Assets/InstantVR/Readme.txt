InstantVR
Version 3.8.8
- Unity 2017.1/2 support
- Fixed Tracker Position being overwritten in UnityVR script
- Added Handedness on the Handles
- Oculus Utilities updated to version 1.19.0
- Minor bugfixes